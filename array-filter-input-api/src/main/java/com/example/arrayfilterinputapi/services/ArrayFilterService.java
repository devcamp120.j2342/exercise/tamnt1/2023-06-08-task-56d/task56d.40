package com.example.arrayfilterinputapi.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

@Service
public class ArrayFilterService {
    public ArrayList<Integer> getRequestQuery(int pos) {
        int[] rainbows = { 1, 23, 32, 43, 54, 65, 86, 10, 15, 16, 18 };
        ArrayList<Integer> filteredArray = new ArrayList<>();

        for (Integer rainbow : rainbows) {
            if (rainbow > pos) {
                filteredArray.add(rainbow);
            }
        }
        return filteredArray;
    }

    public Integer getRequestByIndex(int idx) {
        Integer[] rainbows = { 1, 23, 32, 43, 54, 65, 86, 10, 15, 16, 18 };

        for (int index = 0; index < rainbows.length; index++) {
            if (index == idx && idx > 0 && idx < 10) {
                return rainbows[index];
            }
        }
        return null;
    }
}
