package com.example.arrayfilterinputapi.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.arrayfilterinputapi.services.ArrayFilterService;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class ArrayFilterControllers {
    @Autowired
    ArrayFilterService arrayFilterService;

    @GetMapping("/array-int-request-query")
    public ArrayList<Integer> getQueryInt(@RequestParam int pos) {
        return arrayFilterService.getRequestQuery(pos);
    }

    @GetMapping("/array-int-param/{index}")
    public Integer getQueryByIndex(@PathVariable("index") int index) {
        return arrayFilterService.getRequestByIndex(idx);
    }
}
